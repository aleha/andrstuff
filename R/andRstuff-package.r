#' andRstuff
#'
#' @name andRstuff
#' @docType package
#' @import stats
#' @import plyr
NULL


##' Confidence Interval Around Median
##'
##' Two methods to estimate the confidence interval around the median:
##'   boostrap and quantile of the binomial distribution.
##'
##' Based on answers here:
##' http://stats.stackexchange.com/questions/21103/confidence-interval-for-median
##' @param x numerical vector.
##' @param method charcter in c("qbinom", "bootstrap").  Defaults to "qbinom"
##' @param conf.level numerical. defaults to 0.95
##' @param nboot numerical.  number of boostrap samples to draw.
##'   Defaults to 10^4
##' @param na.rm logical.  defaults to \code{TRUE}
##' @return list with elemet CI.
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' medianCI(1:20)
medianCI <- function(x, method = "qbinom", conf.level = 0.95, nboot = 10^4, na.rm = TRUE)
{
  conf.level.oneside <- (1-conf.level)/2

  if (na.rm)
    x <- na.omit(x)

  if (method == "qbinom") {
    CI <- sort(x)[qbinom(c(conf.level.oneside,1-conf.level.oneside), length(x), 0.5)]
    res <- list(CI = CI,
                conf.levels = conf.level,
                method = method)
  } else if (method == "bootstrap") {
    if (length(x) == 0) {
      CI <- NA
    } else {
      bootmed <- sapply(1:nboot, function(bi) median(sample(x, length(x), replace = TRUE)))
      ##bootsam <- matrix(sample(x, rep=TRUE, nboot*length(x)), nrow=nboot)
      ##bootmed <- apply(bootsam, 1, median)
      CI <- quantile(bootmed, c(conf.level.oneside, 1-conf.level.oneside))
    }
    res <- list(CI = CI,
                conf.levels = conf.level,
                method = method,
                nboot = nboot)
  }

  return(res)
}


##' convert column to row names
##'
##' This is the inverse of add_rownames.  From http://stackoverflow.com/a/36706785
##' @param df data.frame
##' @param col which column to use as row.names
##' @return data.frame with \code{col} dropped and added rownames
##' @author Bob Rudis (@hrbrmstr)
##' @export
##' @examples
##' head(as.matrix(reset_rownames(add_rownames(mtcars), "rowname")))
##' @rdname to_rownames
to_rownames <- function(df, col="rowname") {
  stopifnot(is.data.frame(df))
  col <- as.character(substitute(col))
  to_rownames_(df, col)
}

##' @rdname to_rownames
##' @export
##' @examples
##' m <- "rowname"
##' head(as.matrix(reset_rownames_(add_rownames(mtcars), m)))
to_rownames_ <- function(df, col="rowname") {
  stopifnot(is.data.frame(df))
  nm <- data.frame(df)[, col]
  df <- df[, !(colnames(df) %in% col)]
  rownames(df) <- nm
  df
}

##' remove outliers
##'
##' @param x numeric vector
##' @param method character in c("boxplot", "gauss").  methods for
##'   outlier detection
##' @param ... additional arguments passed to the outlier detection
##'   method
##' @return numeric vector.
##' @author Andreas Leha
##' @export
##' @examples
##' vec <- c(rnorm(10), 4)
##' vec
##' rmOutliers(vec)
rmOutliers <- function(x, method="boxplot", ...)
{
  is_outlier <- switch(method,
                       boxplot = isOutlier(x, ...),
                       gauss   = isOutlierGauss(x, ...))

  x[!is_outlier]
}

##' Mark outliers based on boxplots.stats
##'
##' @param x numeric vector
##' @param direction in c("high", "low", "both")
##' @return logical vector of \code{length(x)}
##' @author  Andreas Leha
##' @export
##' @examples
##' vec <- c(rnorm(10), 4)
##' isOutlier(vec)
isOutlier <- function(x, direction = "both")
{
  o <- x %in% boxplot.stats(x)$out

  if (direction == "high") o <- o & x > mean(x, na.rm=TRUE)
  if (direction == "low")  o <- o & x < mean(x, na.rm=TRUE)

  o
}

##' Mark outliers based on difference from mean
##'
##' Criterion is: difference from mu > sdfactor * sd
##' @param x numeric vector
##' @param sdfactor numeric. see details
##' @param direction in c("high", "low", "both")
##' @return logical vector of \code{length(x)}
##' @author  Andreas Leha
##' @export
##' @examples
##' vec <- c(rnorm(10), 4)
##' isOutlierGauss(vec)
##' isOutlierGauss(vec, sdfactor = 2)
isOutlierGauss <- function(x, sdfactor=3, direction = "both")
{
  mu <- mean(x, na.rm=TRUE)
  sd <-   sd(x, na.rm=TRUE)

  x <- scale(x, scale=FALSE)

  o <- abs(x) > sdfactor * sd

  if (direction == "high") o <- o & x > 0
  if (direction == "low")  o <- o & x < 0

  o <- as.vector(o)

  o
}

##' Standard Error
##'
##' Function to calculate the standard error.  This version omits
##'   missing values.
##' @param x numerical vector
##' @return numerical.  the standard error of \code{x}
##' @author Andreas Leha
##' @export
se <- function(x) sqrt(var(x,na.rm=TRUE)/length(na.omit(x)))


##' Rank Based Inverse Gaussian Tranformation
##'
##' This function performs the rank based inverse gaussian
##'   transformation on a numeric input vector.  \code{x} is first
##'   rank standardized.  Then, the quantiles of the gaussian
##'   distribution at the corresponding probabilities are returned.
##'
##' Missing values are propagated to the output.  That means that
##'   there are less quantiles to be computed if missing values are
##'   present in \code{x}.
##' @param x numeric vector
##' @return numeric vector of length \code{length(x)}
##' @author Andreas Leha
##' @export
##' @examples
##' ## generate some data
##' x <- runif(100)
##' hist(x)
##'
##' ## Rank Based Inverse Gaussianize
##' y <- rbig(x)
##' hist(y)
##'
##' ## the rank is preserved (by construction)
##' identical(rank(x), rank(y))
rbig <- function(x) {
  idx_NA <- is.na(x)
  z <- x[!idx_NA]
  zr <- rank(z)
  p <- zr / (length(zr) + 1)
  zrig <- qnorm(p)
  x[!idx_NA] <- zrig
  return(x)
}


##' Report dim(x) or length(x)
##'
##' @param x data.frame, matrix, vector, list, ....
##' @return numeric.  Returns \code{dim(x)} or \code{length(x)} if
##'   \code{is.null(dim(x))}.
##' @author Andreas Leha
##' @export
##' @examples
##' sapply(list(mtcars, 1:10), dol)
dol <- function(x) {
  d <- dim(x)
  if (is.null(d))
    d <- length(x)

  d
}

##' Drop Cols/Rows in a data.frame Based on NA Count
##'
##' @param data data.frame or matrix.
##' @param dim character in c("cols", "rows") or numeric in {1,2}
##'   where 1 is translated to \code{'rows'} and 2 is translated to
##'   \code{'cols'}
##' @param thresh numeric in [0; 1] or character in c("any", "all")
##'   giving the threshold.  If 'any' the row/col will be dropped if
##'   it contains at least one NA value
##' @param navals vector of values to consider as \code{NA}.  Defaults
##'   to c(NA).  Another sensible value might be c(NA, NaN, Inf) e.g.
##' @return data of reduced dimensions where the NA cols/rows are
##'   dropped.
##' @author Andreas Leha
##' @export
##' @examples
##' ## sample data
##' d <- mtcars
##'
##' ## introduce an NA value
##' d[3,3] <- NA
##' d
##'
##' ## drop the NA col
##' dropNAs(d)
dropNAs <- function(data, dim="cols", thresh="any", navals=c(NA))
{
  ## -------------------------------- ##
  ## clean parameters                 ##
  ## -------------------------------- ##

  ## the dimension: convert to numeric
  if (is.character(dim))
    dim <- switch(dim,
                  rows=1,
                  cols=2,
                  stop("unknown value dim = ", dim))

  ## the threshold
  if (is.character(thresh))
    thresh <- switch(thresh,
                    any=0,
                    all=1)
  if (thresh == 0)
    thresh <- .Machine$double.xmin


  ## -------------------------------- ##
  ## count NAs                        ##
  ## -------------------------------- ##
  countNA <- function(x, navals) sum(x %in% navals)
  freqNA <- function(x, navals) countNA(x, navals)/length(x)
  fNA <- apply(data, dim, freqNA, navals=navals)
  iNA <- fNA > thresh

  if (dim == 1) {
    res <- data[!iNA, ]
  } else if (dim == 2) {
    res <- data[,!iNA]
  } else {
    stop("unsupported value dim = ", dim)
  }

  return(res)
}

##' Summarize a Vetor to One Value
##'
##' Given a vector \code{x} report either \code{mean(x)} (if \code{x}
##'   is \code{numeric} or  \code{x[1]} (if \code{allSame(x)} or \code{NA}.
##' @param x vector to be summarized
##' @param avg_dates logical.  If \code{TRUE}, \code{Date} or \code{POSIXt} types are
##'   treated as numeric (i.e. averaged).  If \code{FALSE},
##'   \code{Date} and \code{POSIXt} variables are treated as
##'   non-numeric and reported via there single unique value or \code{NA}
##' @param na.rm logical. remove \code{NA} from x
##' @return vector of length 1 of the same class as \code{x} (or \code{NA})
##' @author Andreas Leha
##' @export
##' @examples
##' avgUniqueNA(1:10)
##' avgUniqueNA(letters[c(1, 1, 1)])
##' avgUniqueNA(letters[c(1, 2, 3)])
avgUniqueNA <- function(x, avg_dates = FALSE, na.rm=TRUE)
{
  ## return the mean of numerics
  if (is.numeric(x))
    return(mean(x, na.rm = na.rm))

  ## return the mean of Dates
  if (avg_dates && (is(x, "Date") || is(x, "POSIXt")))
    return(mean(x, na.rm = na.rm))

  ## return the single uniqe value if that exists
  if (na.rm) {
    if (allSame(na.omit(x))) return(na.omit(x)[1])
  } else {
    if (allSame(x)) return(x[1])
  }

  ## return (class specific!) NA otherwise
  if (is.character(x))
    return(NA_character_)
  else if (is.integer(x))
    return(NA_integer_)
  else if (is.complex(x))
    return(NA_complex_)
  else
    return(NA)
}

##' Reduce a data.frame to a Single Row
##'
##' For each column of the given \code{data.frame} \code{x} this
##'   function will return 1 value: the mean for numerical columns,
##'   the single unique value for other columns if that exists, or NA
##'   otherwise.
##' @param x data.frame to reduce
##' @param append_n logical or character.  append a column giving the
##'   number of rows in \code{x}?  If of class character used as name
##'   for that new column.
##' @param avg_dates logical.  If \code{TRUE}, \code{Date} or \code{POSIXt} types are
##'   treated as numeric (i.e. averaged).  If \code{FALSE},
##'   \code{Date} and \code{POSIXt} variables are treated as
##'   non-numeric and reported via there single unique value or \code{NA}
##' @param na.rm logical. remove \code{NA} prior to averaging?
##' @return data.frame with one row only
##' @author Andreas Leha
##' @export
##' @examples
##' tmp <- data.frame(a=1:10, b=letters[1:10], c=letters[rep(1, 10)])
##' tmp
##' reduceOneRow(tmp)
reduceOneRow <- function(x, append_n = TRUE, avg_dates = FALSE, na.rm = TRUE)
{
  y <- dplyr::summarize_each(x, dplyr::funs(avgUniqueNA(., avg_dates=avg_dates, na.rm = na.rm)))

  if (!is.logical(append_n) || append_n)
    y <- dplyr::mutate_(y, .dots = list(n=~n()))

  if (!is.logical(append_n))
    data.table::setnames(y, "n", append_n)

  return(y)
}

##' convenience function to quickly predict residuals
##'
##' @param mod model which has to have an associated predict S3 method
##' @param dataf data.frame with the (new) data
##' @param var column specification of the new data
##' @param ... passed on to \code{predict}
##' @return resuduals
##' @author Andreas Leha
##' @export
predictResiduals <- function(mod, dataf, var, ...)
{
  dataf[,var] - predict(mod, newdata=dataf, ...)
}

##' make dataframe 'factor-less'
##'
##' loops over all columns of \code{dframe} and converts everthing
##' that \code{is.factor} to \code{character}
##' @param dframe data.frame
##' @return data.frame.
##' @author Andreas Leha
##' @export
##' @examples
##' data(iris)
##' str(iris)
##'
##' iris2 <- dfFactorsToCharacter(iris)
##' str(iris2)
dfFactorsToCharacter <- function(dframe)
{
  for (j in 1:ncol(dframe)) {
    if (is.factor(dframe[,j])) {
      dframe[,j] <- as.character(dframe[,j])
    }
  }
  return(dframe)
}


##' Match and Extract Regular Expression
##'
##' This is a rough regular expression extraction function.
##' Basically, it runs \code{regexec} followed by \code{regmatches}.
##' It always returns the last match found by \code{regexec} to allow
##' for patterns with paretheses.
##' @param pattern character. a pattern e.g. "\\((.+)\\)"
##' @param x character vector.  (vector of) character from which to
##' extract the pattern matches
##' @return character vector.  NA for positions without match, the
##' last match found by \code{regexec} for position with match
##' @author Andreas Leha
##' @export
##' @examples
##' al_regextract("\\((.+)\\)", "text (with note)")
al_regextract <- function(pattern, x)
{
  m <- regexec(pattern, x)
  ret <- regmatches(x, m)
  ret <- lapply(ret, last)
  ret <- lapply(ret, function(y) ifelse(length(y)==0, NA, y))
  ret <- unlist(ret)
  return(ret)
}


##' Detatch a package.
##'
##' R's syntax to get rid of a package is odd and hard to remember.
##' This function from
##' \href{http://stackoverflow.com/a/6979989}{stackoverflow} solves
##' these issues.
##' @param pkg character or symbol with name of the packaget (e.g. "andRstuff")
##' @param character.only needs to be true if package name is passed
##' as character
##' @return none
##' @author kohske
##' @export
##' @examples
##' \dontrun{
##' detach_package(vegan)
##' detach_package("vegan", TRUE)
##' }
detach_package <- function(pkg, character.only = FALSE)
{
  if(!character.only)
  {
    pkg <- deparse(substitute(pkg))
  }
  search_item <- paste("package", pkg, sep = ":")
  while(search_item %in% search())
  {
    detach(search_item, unload = TRUE, character.only = TRUE)
  }
}

##' Test whether all elements of a vector are equal
##'
##' This test is for exact equality.  For numeric comparisons, you'll
##' most likely want something else (See also
##' \href{http://cran.r-project.org/doc/FAQ/R-FAQ.html#Why-doesn_0027t-R-think-these-numbers-are-equal_003f}{FAQ 7.31})
##'
##' Based on
##' \href{https://stat.ethz.ch/pipermail/r-help/2005-August/078128.html}{post} to R-help by Vincent Goulet.
##' @param v vector to test
##' @param na.rm logical. if FALSE (default) NAs in v will propagate
##' to the result of allSame
##' @return logical
##' @author Andreas Leha
##' @export
##' @examples
##' allSame(c(NA, 1, 1))
##' allSame(c(NA, 1, 1), na.rm=TRUE)
allSame <- function(v, na.rm=FALSE)
{
  all(v == na.omit(v)[1], na.rm=na.rm)
}

##' Test Wheter the Given Vectors are Equal Element-wise
##'
##' Call \code{rbind} on the given vectors and then call
##' \code{\link{allSame}} on all columns.
##' @param ... the vectors.  Assumed to be of equal length
##' @param na.rm passed to \code{\link{allSame}} and \code{all}
##' @return logical
##' @author Andreas Leha
##' @export
##' @examples
##' allSameVecElementwise(c(1, 0), c(1, 0))
##' allSameVecElementwise(c(1, 1), c(1, 0))
##' allSameVecElementwise(c(1, NA), c(1, 0))
##' allSameVecElementwise(c(1, NA), c(1, 0), na.rm = TRUE)
##' allSameVecElementwise(c(0, NA), c(1, 0), na.rm = TRUE)
allSameVecElementwise <- function(..., na.rm=FALSE)
{
  vs <- list(...)
  vm <- do.call(rbind, vs)

  all_same_pos <- apply(vm, 2, allSame, na.rm=na.rm)

  all(all_same_pos, na.rm=na.rm)
}




##' Cast data.frames to wide format -- even with more value columns
##'
##' This function is built on plyr and is, thus, quite slow.  But it
##' provides the nice functionalty to cast a data.frame into wide
##' format along a 'sorting variable'.  Other then that it provides
##' less flexibility compared to dcast from the reshape2 package.
##' @param dataf data.frame.  The data to cast
##' @param id.vars character vector.  columns that identify rows,
##' i.e. row that should not change
##' @param sort.vars character vector.  Giving columns which are used
##' to cast.
##' @param val.vars character vector.  These columns will be repeated
##' for every value of the sort.var.
##' @param sortonNA logical.  If TRUE missing values in sort.vars will
##' generate columns in the casted version.
##' @param prepend logical.  If TRUE the names of the new value
##' columns will start with the value of the sort.var.  If FALSE (the
##' default) they will end with that value.
##' @return data.frame.  dataf in wide format
##' @author Andreas Leha
##' @export
castWide <- function(dataf,
                     id.vars,
                     sort.vars,
                     val.vars,
                     sortonNA=FALSE,
                     prepend=FALSE)
{
  if (missing(val.vars)) {
    val.vars <- setdiff(colnames(dataf), c(id.vars, sort.vars))
    message("using ", paste(val.vars, collapse = ","), " as values")
  }

  unused.vars <- setdiff(colnames(dataf), c(id.vars, sort.vars, val.vars))
  if (length(unused.vars) > 0) {
    warning("not using ", paste(unused.vars, collapse = ","))
  }

  res <-
    ddply(dataf, id.vars, function(x) {
            xss <- x[,!colnames(x) %in% setdiff(id.vars, sort.vars)]
            ##xss <- x[,colnames(x) %in% unique(c(sort.vars, val.vars))]
            ##browser()
            xl <-
              dlply(xss, sort.vars, function(y) {
                      svar <- y[1, sort.vars]
                      if (!any(is.na(svar)) | sortonNA) {
                        svart <- paste(svar, collapse="_")
                        ##y <- y[,!colnames(y) %in% sort.vars]
                        y <- y[,val.vars,drop=FALSE]
                        y <- y[!duplicated(y),]
                        if (prepend) {
                          colnames(y) <- paste0(svart, "_", colnames(y))
                        } else {
                          colnames(y) <- paste0(colnames(y), "_", svart)
                        }
                        return(y)
                      } else {
                        return(data.frame(castWideDummy=NA))
                      }
                    })
            Reduce(cbind, xl)
          })

  res <- res[,!colnames(res) %in% "castWideDummy"]

  return(res)
}

##' Brighten colours
##'
##' Convert given colours to hsv space an apply a factor to the 'v' value
##' @param col the colour in rgb space (e.g. "#FFF3C8")
##' @param howmuch the factor to apply the value (i.e. lightness).  to acually
##' brighten the colour use any values above 1.  Defaults to 1.3
##' @param reapplyalpha logical. if TRUE the original value of the
##' alpha channel is reapplied after the brightening (of the original
##' color without transparency)
##' @return brightened colour
##' @author Andreas Leha
##' @export
##' @examples
##' ## set up a test color
##' tcs <- "#00641b"
##' ## brighen
##' pal(c(tcs,
##'       brightenColor(tcs, 1.3),  ## the default
##'       brightenColor(tcs, 0.8))) ## darken
brightenColor <- function(col, howmuch=1.3, reapplyalpha=TRUE) {
  sapply(col, brightenColor_single, howmuch=howmuch, reapplyalpha=reapplyalpha)
}

##' Brighten a single colour (internally only)
##'
##' Convert a given colour to hsv space an apply a factor to the 'v' value
##' @param col the colour in rgb space (e.g. "#FFF3C8")
##' @param howmuch the factor to apply the value (i.e. lightness).  to acually
##' brighten the colour use values above 1.  Defaults to 1.3
##' @param reapplyalpha logical. if TRUE the original value of the
##' alpha channel is reapplied after the brightening (of the original
##' color without transparency)
##' @return brightened colour
##' @author Andreas Leha
brightenColor_single <- function(col, howmuch=1.3, reapplyalpha=TRUE) {
  ## extract alpha
  alpha <- col2rgb(col, alpha = TRUE)["alpha",1]
  alpha <- alpha / 255

  hsv_values <- rgb2hsv(col2rgb(col))
  brighter_hsv_values <- hsv_values * c(1, 1, howmuch)
  brighter_hsv_values[2:3] <- pmin(1, brighter_hsv_values[2:3])

  res <- hsv(brighter_hsv_values[1],
             brighter_hsv_values[2],
             brighter_hsv_values[3])

  if (reapplyalpha) {
    res <- addAlphaChannel(res, alpha)
  }

  return(res)
}

##' Add Transparency to given Colours
##'
##' @param col the (vector of) colour(s).  (e.g. "#FFF3C8")
##' @param alpha numeric.  Value to pre-fill the alpha channel.
##' @return colour with transparency in alpha channel
##' @author Andreas Leha
##' @export
##' @examples
##' ## set up some test colors
##' tcs <- c("#ffd649", "#00641b")
##' ## add transparency
##' pal(c(tcs,
##'       addAlphaChannel(tcs, 0.3), ## the default
##'       addAlphaChannel(tcs, 0.8)))
addAlphaChannel <- function(col, alpha=0.3) {
  sapply(col, addAlphaChannel_single, alpha=alpha)
}


##' Add Transparency to a Single Colour (internal only)
##'
##' @param col the colour.  (e.g. "#FFF3C8")
##' @param alpha numeric.  Value to pre-fill the alpha channel.
##' @return colour with transparency in alpha channel
##' @author Andreas Leha
addAlphaChannel_single <- function(col, alpha=0.3) {
  hsv_values <- rgb2hsv(col2rgb(col))

  hsv(hsv_values[1],hsv_values[2],hsv_values[3], alpha=alpha)
}

##' Blend Colors Together
##'
##' @param colors vector of colors
##' @return single colour blended from colors
##' @author Andreas Leha
##' @export
##' @examples
##' ## set up some test colors
##' tcs <- c("#ffd649", "#00641b")
##' ## add some transperancy
##' tcs <- addAlphaChannel(tcs, 0.7)
##' ## blend
##' pal(c(tcs, blendColors(tcs)))
blendColors <- function(colors) {
  mixed_color_rgb <- col2rgb("white", alpha=1)
  for (new_col in colors) {
    new_color_rgb <- col2rgb(new_col,alpha=TRUE)
    if(sum(rownames(new_color_rgb)=="alpha")==0) {
      new_color_rgb <- rbind(new_color_rgb,alpha=255)
    }
    alpha <- new_color_rgb["alpha",1]/255
    mixed_color_rgb <- (1-alpha)*mixed_color_rgb + (alpha)*new_color_rgb
  }
  ##mixed_color_rgb["alpha",1] <- 255
  rgb(red=mixed_color_rgb["red",1],
      blue=mixed_color_rgb["blue",1],
      green=mixed_color_rgb["green",1],
      maxColorValue=255)
}



##' setdiff for data.frames
##'
##' Ignoring rownames, this function returns the rows in df1 that are
##' not present in df2.
##' Credit goes to anonymus via http://stackoverflow.com/a/16144262
##' No safety (dimension, class) checks!
##' @param df1 data.frame
##' @param df2 data.frame
##' @return data.frame containing the diff lines
##' @author Andreas Leha
##' @export
dfdiff <- function(df1, df2)
{
  df1[!duplicated(rbind(df2, df1))[-seq_len(nrow(df2))], ]
}

##' Run as.character on every factor in a data.frame
##'
##' This function will do as.character on every factor in a
##' data.frame.
##' @param thedf data.frame.  The only argument.
##' @return data.frame with factors converted to characters.
##' @author Andreas Leha
##' @export
unfactorize <- function(thedf)
{
  for (j in 1:ncol(thedf)) {
    if (is.factor(thedf[,j]))
      thedf[,j] <- as.character(thedf[,j])
  }
  thedf
}


##' Open file using a specified command
##'
##' This just calls \code{\link[base]{system}} on the pasted command +
##' filename.  Additionally, it (optionally) checks for the file to
##' exist.
##'
##' @param fl character.  the name (plus path) of (to) the file to be
##' opened
##' @param cmd character.  the command to run on that file
##' @param check logical.  if TRUE, the \code{fl} is cheched for
##' existance prior to the command execution.  Defaults to FALSE.
##' @param verbose logical.  write something or not.
##' @export
##' @examples
##' \dontrun{
##' openWith(system.file("DESCRIPTION"), "emacsclient")
##' openWith("DTRNEUuia", "emacsclient", check = TRUE)
##' }
openWith <- function(fl, cmd, check=FALSE, verbose=TRUE)
{
  fl <- as.character(fl)

  if (check) {
      if (!file.exists(fl)) stop("file ", fl, " does not exist.")
    }

  cmd <- paste(cmd, paste0("\"", gsub("^~/", "$HOME/", fl), "\""))

  if (verbose) message(cmd)

  system(cmd, wait = FALSE, ignore.stdout=!verbose, ignore.stderr=!verbose)
}

##' @rdname openWith
##' @export
sxiv <- function(fl, cmd="sxiv", check=TRUE, verbose=TRUE)
{
  openWith(fl, cmd=cmd, check=check, verbose=verbose)
}

##' @rdname openWith
##' @export
llpp <- function(fl, cmd="llpp", check=TRUE, verbose=TRUE)
{
  openWith(fl, cmd=cmd, check=check, verbose=verbose)
}

##' @rdname openWith
##' @export
genopen <- function(fl, cmd=NULL, check=TRUE, verbose=TRUE)
{
  ## use default command "xdg-open"
  if (is.null(cmd)) {
    ## use "open" instead of xdg-open in osx
    if (grepl("darwin", R.version$platform, ignore.case = TRUE)) {
      cmd <- "open"
    } else {
      cmd <- "xdg-open"
    }
  }

  openWith(fl, cmd=cmd, check=check, verbose=verbose)
}

##' @rdname openWith
##' @export
email <- function(fl, cmd=NULL, check=TRUE, verbose=TRUE)
{
  ## use default command "xdg-open"
  if (is.null(cmd)) {
    ## use "open" instead of xdg-open in osx
    if (grepl("darwin", R.version$platform, ignore.case = TRUE)) {
      cmd <- "open -a Thunderbird"
    } else {
      cmd <- "xdg-open"
    }
  }

  ## does not know about getwd?
  fl <- makePathAbsolute(fl)

  openWith(fl, cmd=cmd, check=check, verbose=verbose)
}

makePathAbsolute <- function(fl)
{
  if (!grepl("^/", fl)) {
    file.path(getwd(), fl)
  } else {
    fl
  }
}


##' Print a Vector and mark one element
##'
##' Prints two rows.  First row has the elements in the vector.
##' Second row contains a marker marking the given element.
##'
##' In case the given element is more than once in the vector, the
##' first occurence will be marked.
##' @param elem an elenemnt of the vector.  The first occurence of this
##' will be marked.
##' @param vec vector.  the vector to print.
##' @return invisible matrix.
##' side effect is the printing.
##' @author Andreas Leha
##' @export
##' @examples
##' markValueInVector(20, seq(10, 100, 10))
markValueInVector <- function(elem, vec)
{
  if (!elem %in% vec)
      stop("'", elem, "' is no element of the given vector")

  markrow <- rep("", length(vec))
  markrow[which(vec == elem)[1]] <- "\u2191"
  toprint <- rbind(vec, markrow)

  if (is.null(names(vec))) {
    colnames(toprint) <- rep("", ncol(toprint))
  } else {
    colnames(toprint) <- names(vec)
  }
  rownames(toprint) <- rep("", nrow(toprint))

  print(toprint, quote=FALSE, right=TRUE)
}

##' make a data.frame with given vectors as columns.  Pad shorter vectors
##' with 'filling'.
##'
##' @title make a data.frame from vectors of (possibly) different length
##' @param ... vectors to be cbinded
##' @param filling this is used to fill shorter vectors. recycled if necessary
##' @return data.frame
##' @export
##' @author Andreas Leha
veccbind.fill <- function(..., filling="") {
  bindlist <- list(...)

  maxlength <- max(laply(bindlist, length))

  paddedlist <-
    llply(bindlist,
          function(vec, length) c(vec, rep(filling, maxlength-length(vec))),
          maxlength)

  retdf <- as.data.frame(paddedlist)
  names(retdf) <- names(bindlist)

  retdf
}

##' cbinds matrices.  Relies on rownames.  missing values are filled with NA
##'
##' @title filling cbind for matrices
##' @param ... the matrices to be cbind'ed
##' @return bigger matrix
##' @author Andreas Leha
##' @export
matcbind.fill <- function(...) {
  bindlist <- list(...)

  rescolnames <- unlist(lapply(bindlist, colnames))
  resrownames <- unique(unlist(lapply(bindlist, rownames)))

  res <-
    matrix(NA,
           nrow=length(resrownames),
           ncol=length(rescolnames))

  rownames(res) <- resrownames
  colnames(res) <- rescolnames

  for(mat in bindlist) {
    res[rownames(mat), colnames(mat)] <- mat
  }

  res
}



##' list objects with size and dimension information
##'
##' @title improved list of objects
##' @param pattern character.  pattern to search for to restrict the output
##' @param order.by charchter.  which column to sort by.  in c("Type", "Size", "PrettySize", "Rows", "Columns")
##' @param show.functions boolean.  report functions as well?  defaults to FALSE
##' @param pos integer. which envirenment to look at (number in \code{search()})
##' @param decreasing boolean.  which way around to order.
##' @param head boolean.  only report the top rows?
##' @param n integer. how many rows to report if head==TRUE
##' @return data.frame with the columns c("Type", "Size", "PrettySize", "Rows", "Columns")
##' @author from the web
##' @export
ls.objects <- function (pattern, order.by, show.functions=FALSE, pos = 1,
                        decreasing=FALSE, head=FALSE, n=5) {
    napply <- function(names, fn) sapply(names, function(x)
                                         fn(get(x, pos = pos)))
    names <- ls(pos = pos, pattern = pattern)
    if (length(names) == 0) {
      message("no objects")
      return(data.frame(Type=NULL, Size=NULL, PrettySize=NULL, Rows=NULL, Columns=NULL))
    }
    obj.class <- napply(names, function(x) as.character(class(x))[1])
    obj.mode <- napply(names, mode)
    obj.type <- ifelse(is.na(obj.class), obj.mode, obj.class)
    obj.prettysize <-
      napply(names, function(x) {
        capture.output(print(object.size(x), units = "auto")) })
    obj.size <- napply(names, object.size)
    obj.dim <- t(napply(names, function(x)
                        as.numeric(dim(x))[1:2]))
    vec <- is.na(obj.dim)[, 1] & (obj.type != "function")
    obj.dim[vec, 1] <- napply(names, length)[vec]
    out <- data.frame(obj.type, obj.size, obj.prettysize, obj.dim)
    names(out) <- c("Type", "Size", "PrettySize", "Rows", "Columns")
    if (!missing(order.by))
        out <- out[order(out[[order.by]], decreasing=decreasing), ]
    if (head)
        out <- head(out, n)

    if (!show.functions)
        out <- out[out$Type != "function",]

    out
}

##' shorthand to call ls.objects with standart arguments
##'
##' @title improved list of objects
##' @param ... will be passed to ls.objects
##' @param n integer.  how many rows (i.e. objects) to return
##' @return data.frame.  returned from ls.objects
##' @author from the web
##' @export
lsos <- function(..., n=10) {
    ls.objects(..., order.by="Size", decreasing=TRUE, head=TRUE, n=n)
}



##' prepend numbers with leading 0s
##'
##' Useful for numbering of samples, patients, files, etc.
##' @title padIntVector
##' @param vec vector, that will be converted by as.character
##' @param paddlen int. up to which length add leading 0s
##' @return padded vector
##' @author Andreas Leha
##' @export
##' @examples
##' padIntVector(1:5, 2)
padIntVector <- function(vec, paddlen) {
  if (missing(paddlen)) {
    paddlen <- nchar(as.character(max(vec)))
  }
  sprintf(paste("%0",paddlen,"d",sep=""), vec)
}

##' write a data.frame into an docx file
##'
##' The function is a wrapper, that makes it easy to write data.frames
##' into xlsx files.
##' @param x data.frame to write
##' @param filename character of output file.
##' @return none
##' @author Dr. Andreas Leha
##' @export
alwrite.df2docx <- function(x, filename) {
    ## library("officer")
    doc <- officer::read_docx()
    doc <- officer::body_add_table(doc, x, style = "table_template")
    officer:::print.rdocx(doc, target = filename)
}

##' write a data.frame into an xlsx file
##'
##' The function is a wrapper, that makes it easy to write data.frames
##' into xlsx files.  As additional feature, it allows the conversion of
##' org-bold syntax into colored background cells.  This is very useful for
##' the reporting of p-values
##' @name alwrite.xlsx
##' @param df data.frame to write
##' @param filename character of output file.  WILL BE OVERWRITTEN
##' @param orgbold boolean.  put entries like *foo* into colored cells and strip the asterixs
##' @param colwidths vector (recycled) of length ncol(df).  See
##' \code{\link[openxlsx]{writeData}} for details.
##' @param headerstyle Style as returned from
##' \code{\link[openxlsx]{createStyle}} to be applied to the header row.
##' @param append logical.  if TRUE new tables are added to an
##' existing workbook
##' @return none
##' @author Andreas Leha
##' @export
##' @examples
##' ## generate some data
##' pval_df <- data.frame(a=1:2,
##'                       b=letters[15:16],
##'                       p=paste0("*", c(0.04, 0.01), "*"))
##'
##' ## generate a filename
##' al_file <- file.path(tempdir(), "al_file.xlsx")
##'
##' ## write the data
##' alwrite.xlsx(pval_df, al_file, orgbold = TRUE)
##'
##' \dontrun{
##' ## check
##' genopen(al_file)
##' }
alwrite.xlsx <- function(df, filename, orgbold=FALSE, colwidths="auto", headerstyle, append = FALSE) {

  if (append && file.exists(filename)) {
    wb <- openxlsx::loadWorkbook(filename)
  } else {
    wb <- openxlsx::createWorkbook()
  }

  ## header style
  if (missing(headerstyle))
    headerstyle <- openxlsx::createStyle(textDecoration = "BOLD",
                                         fontColour = "#FFFFFF",
                                         fontSize=12,
                                         ##fontName="Arial Narrow",
                                         fgFill = "#4F80BD")

  if (identical(NULL, names(df)))
    names(df) <- 1:length(df)

  if (is.list(df) && !is.data.frame(df)) {
    for (myname in names(df)) {

      myname_short <- abbreviate(myname, minlength = 28, strict = TRUE) ## excel limitation on length of sheet names
      myname_short <- gsub("\\$", "_", myname_short)

      alwrite.xlsx.addworksheet(df = df[[myname]],
                                wb = wb,
                                myname = myname_short,
                                orgbold = orgbold,
                                colwidths = colwidths,
                                headerstyle = headerstyle)

    }
  } else {
    myname <- deparse(substitute(df))
    myname_short <- abbreviate(myname, minlength = 28, strict = TRUE) ## excel limitation on length of sheet names
    myname_short <- gsub("\\$", "_", myname_short)

    alwrite.xlsx.addworksheet(df = df,
                              wb = wb,
                              myname = myname_short,
                              orgbold = orgbold,
                              colwidths = colwidths,
                              headerstyle = headerstyle)
  }

  openxlsx::saveWorkbook(wb, filename, overwrite = TRUE)
}

alwrite.xlsx.addworksheet <- function(df, wb, myname, orgbold, colwidths, headerstyle)
{
  openxlsx::addWorksheet(wb, sheetName=myname)

  if (!orgbold) {
    openxlsx::writeData(wb, myname, df, headerStyle = headerstyle)
  } else {
    tmp <- df
    for (j in 1:ncol(tmp)) tmp[,j] <- sub("^\\*", "", sub("\\*$", "", tmp[,j]))
    openxlsx::writeData(wb, myname, tmp, headerStyle = headerstyle)

    ## First, create a corresponding (named) cell style
    signifStyle <- openxlsx::createStyle(fgFill="#FF4900")

    for (j in 1:ncol(df)) {
      rowindex <- grep("^\\*", df[,j]) + 1
      if (length(rowindex) > 0) {
        ## Set the 'signifStyle' cell style for the corresponding cells.
        openxlsx::addStyle(wb, sheet=myname, style=signifStyle, rows=rowindex, cols=rep(j, length(rowindex)))
      }
    }
  }

  openxlsx::setColWidths(wb, sheet = myname, cols = 1:ncol(df), widths = colwidths)
}

##' transpose numeric data.frames
##'
##' Transposes numeric data.frames and handles row and col names correctly
##' @name transposeDF
##' @param df data.frame to transpose.  will be handeled with as.numeric
##' @return t(df)
##' @author Andreas Leha
##' @export
transposeDF <- function(df) {
  colnames <- colnames(df)
  tmp <- t(df)
  tmp <- as.data.frame(tmp)
  tmp <- lapply(tmp, function(x) as.numeric(as.character(x)))
  tmp <- as.data.frame(tmp)
  rownames(tmp) <- colnames
  tmp
}

##' rbind ignoring colnames
##'
##' This version of rbind assumes, that the order is correct and performs
##' an rbind even with missing or non-matching colnames
##' @name myrbind
##' @param df data.frame to rbind to
##' @param row list, vector, data.frame.  Will be handled by as.data.frame
##' @return rbind(df, row)
##' @author Andreas Leha
##' @export
myrbind <- function(df, row) {
  row <- as.data.frame(row)
  colnames(row) <- colnames(df)

  ret <- rbind(data.frame(lapply(as.data.frame(df),
                                 function(x) if(is.factor(x)) as.character(x) else x),
                          check.names=FALSE,
                          stringsAsFactors=FALSE),
               row)

  data.frame(lapply(ret,unlist), check.names=FALSE)
}

##' Reverse the Levels of a Factor
##'
##' @param fv factor
##' @return factor \code{fv} with reversed levels
##' @author Andreas Leha
##' @export
revlevels <- function(fv)
{
  factor(fv, levels=rev(levels(fv)))
}

##' reverts the values (not lables only!) of an ordered factor
##'
##' Useful for scores - reverts the order: values and labels of an ordered
##' factor
##' @name revOrdered
##' @param fv the factor to be reverted
##' @return a factor with reverted values
##' @author Andreas Leha
##' @export
revOrdered <- function(fv) {
  new_fv <- fv
  nlevels <- length(levels(fv))

  for (i in 1:nlevels) {
    new_fv[fv == levels(fv)[i]] <- levels(fv)[(nlevels + 1) - i]
  }
  new_fv
}


text.pvclust <- function(x, horiz=FALSE, col = c(2, 3, 8), print.num = TRUE, float = 0.01,
    cex = NULL, font = NULL, ...)
{
    axes <- pvclust:::hc2axes(x$hclust)
    if (horiz) {
      axes2 <- axes
      axes2[,1] <- axes[,2]
      axes2[,2] <- axes[,1]
      axes <- axes2
      rm(axes2)
    }
    usr <- par()$usr
    wid <- usr[4] - usr[3]
    hei <- usr[2] - usr[1]
    au <- as.character(round(x$edges[, "au"] * 100))
    bp <- as.character(round(x$edges[, "bp"] * 100))
    rn <- as.character(row.names(x$edges))
    au[length(au)] <- "au"
    bp[length(bp)] <- "bp"
    rn[length(rn)] <- "edge #"
    if (!horiz) {
      x <- axes[, 1]
      yau <- axes[, 2] + float * wid
      ybp <- yau
    } else {
      x <- axes[, 1]
      yau <- axes[, 2] - 0.05 * par()$cra[1]
      ybp <- axes[, 2] + 0.05 * par()$cra[1]
    }
    a <- text(x = x, y = yau, au,
              col = col[1], pos = ifelse(horiz, 2, 2), offset = ifelse(horiz, 0.6, 0.3), cex = cex, font = font,
              srt=ifelse(horiz, 90, 0))
    a <- text(x = x, y = ybp, bp,
        col = col[2], pos = ifelse(horiz, 4, 4), offset = ifelse(horiz, -0.6, 0.3), cex = cex, font = font,
              srt=ifelse(horiz, 90, 0))
    if (print.num)
        a <- text(x = axes[, 1] + 0.5 * float * hei, y = axes[, 2], rn, col = col[3],
            pos = 1, offset = ifelse(horiz, 0, 0.3), cex = cex, font = font,
              srt=ifelse(horiz, 90, 0))
}

pvrect <- function (x, horiz = FALSE, alpha = 0.95, pv = "au", type = "geq", max.only = TRUE,
    border = 2, ...)
{
    len <- nrow(x$edges)
    member <- pvclust:::hc2split(x$hclust)$member
    order <- x$hclust$order
    usr <- par("usr")
    xwd <- usr[2] - usr[1]
    ywd <- usr[4] - usr[3]
    cin <- par()$cin
    ht <- c()
    j <- 1
    if (is.na(pm <- pmatch(type, c("geq", "leq", "gt", "lt"))))
        stop("Invalid type argument: see help(pvrect)")
    for (i in (len - 1):1) {
        if (pm == 1)
            wh <- (x$edges[i, pv] >= alpha)
        else if (pm == 2)
            wh <- (x$edges[i, pv] <= alpha)
        else if (pm == 3)
            wh <- (x$edges[i, pv] > alpha)
        else if (pm == 4)
            wh <- (x$edges[i, pv] > alpha)
        if (wh) {
            mi <- member[[i]]
            ma <- match(mi, order)
            if (max.only == FALSE || (max.only && sum(match(ma,
                ht, nomatch = 0)) == 0)) {
                xl <- min(ma)
                xr <- max(ma)
                yt <- x$hclust$height[i]
                if (horiz) {
                  yb <- usr[2]
                  mx <- ywd/length(member)/3
                  my <- xwd/200
                } else {
                  yb <- usr[3]
                  mx <- xwd/length(member)/3
                  my <- ywd/200
                }
                if (horiz) {
                  rect(yt + my - 0.01 * xwd, xl - mx, yb, xr + mx , border = border,
                       shade = NULL, ...)
                } else {
                  rect(xl - mx, yb + my, xr + mx, yt + my, border = border,
                       shade = NULL, ...)
                }
                j <- j + 1
            }
            ht <- c(ht, ma)
        }
    }
}

##' heatmap.2 with simplified key (rawkey=TRUE) option
##'
##' @return see heatmap.2
##' @author Andreas Leha
##' @param x see heatmap.2
##' @param Rowv see heatmap.2
##' @param Colv see heatmap.2
##' @param distfun see heatmap.2
##' @param hclustfun see heatmap.2
##' @param dendrogram see heatmap.2
##' @param symm see heatmap.2
##' @param scale see heatmap.2
##' @param na.rm see heatmap.2
##' @param revC see heatmap.2
##' @param add.expr see heatmap.2
##' @param breaks see heatmap.2
##' @param symbreaks see heatmap.2
##' @param col see heatmap.2
##' @param colsep see heatmap.2
##' @param rowsep see heatmap.2
##' @param sepcolor see heatmap.2
##' @param sepwidth see heatmap.2
##' @param cellnote see heatmap.2
##' @param notecex see heatmap.2
##' @param notecol see heatmap.2
##' @param na.color see heatmap.2
##' @param trace see heatmap.2
##' @param tracecol see heatmap.2
##' @param hline see heatmap.2
##' @param vline see heatmap.2
##' @param linecol see heatmap.2
##' @param margins see heatmap.2
##' @param ColSideColors see heatmap.2
##' @param RowSideColors see heatmap.2
##' @param cexRow see heatmap.2
##' @param cexCol see heatmap.2
##' @param lasRow las for row lables.  see \code{\link{mtext}}
##' @param lasCol las for col lables.  see \code{\link{mtext}}
##' @param lineRow line to put the row lables.  see \code{\link{mtext}}
##' @param lineCol line to put the col lables.  see \code{\link{mtext}}
##' @param labRow see heatmap.2
##' @param labCol see heatmap.2
##' @param key see heatmap.2
##' @param rawkey character.  in c("", "single", "double").  If "" use the default behaviour.  If "single", plot a key as simple as possible.  If "double", plot a key as simple as possible but make room for a double line title.
##' @param keysize see heatmap.2
##' @param keytitle optional character.  title for the key.
##' @param density.info see heatmap.2
##' @param denscol see heatmap.2
##' @param symkey see heatmap.2
##' @param densadj see heatmap.2
##' @param main see heatmap.2
##' @param xlab see heatmap.2
##' @param cex.xlab cex.xlab added to the argument list
##' @param col.xlab col.xlab added to the argument list
##' @param ylab see heatmap.2
##' @param cex.ylab cex.ylab added to the argument list
##' @param col.ylab col.ylab added to the argument list
##' @param ylableft character.  Printed left of the left dendrogram.
##' @param xlabtop character.  Printed on top of the top dendrogram.
##' @param cex.ylableft numeric.  expansion for ylableft.
##' @param cex.xlabtop numeric. expansion for xlabtop.
##' @param lmat see heatmap.2
##' @param lhei see heatmap.2
##' @param lwid see heatmap.2
##' @param ... see heatmap.2
heatmap.2 <- function (x, Rowv = TRUE, Colv = if (symm) "Rowv" else TRUE,
            distfun = dist, hclustfun = hclust, dendrogram = c("both",
                                                  "row", "column", "none"), symm = FALSE, scale = c("none",
                                                                                            "row", "column"), na.rm = TRUE, revC = identical(Colv,
                                                                                                                              "Rowv"), add.expr, breaks, symbreaks = min(x < 0, na.rm = TRUE) ||
            scale != "none", col = "heat.colors", colsep, rowsep,
            sepcolor = "white", sepwidth = c(0.05, 0.05), cellnote, notecex = 1,
            notecol = "cyan", na.color = par("bg"), trace = c("column",
                                                      "row", "both", "none"), tracecol = "cyan", hline = median(breaks),
            vline = median(breaks), linecol = tracecol, margins = c(5,
                                                          5), ColSideColors, RowSideColors, cexRow = 0.2 + 1/log10(nr),
            cexCol = 0.2 + 1/log10(nc), lasRow=1, lasCol=1, lineRow=0.5, lineCol=1, labRow = NULL, labCol = NULL,
            key = TRUE, rawkey = "", keysize = 1.5, keytitle = NULL, density.info = c("histogram",
                                         "density", "none"), denscol = tracecol, symkey = min(x <
                                                                                   0, na.rm = TRUE) || symbreaks, densadj = 0.25, main = NULL,
            xlab = NULL, cex.xlab=NA, col.xlab="black", ylab = NULL, cex.ylab=NA, col.ylab="black",
                       ylableft=NULL, xlabtop=NULL,
                       cex.ylableft=0.5, cex.xlabtop=0.5,
                       lmat = NULL, lhei = NULL, lwid = NULL,
            ...)
{
  if (!packageAvailable("gtools"))
    stop("package 'gtools' is required for heatmap.2")

  scale01 <- function(x, low = min(x), high = max(x)) {
    x <- (x - low)/(high - low)
    x
  }
  retval <- list()
  scale <- if (symm && missing(scale))
    "none"
  else match.arg(scale)
  dendrogram <- match.arg(dendrogram)
  trace <- match.arg(trace)
  density.info <- match.arg(density.info)
  if (length(col) == 1 && is.character(col))
    col <- get(col, mode = "function")
  if (!missing(breaks) && (scale != "none"))
    warning("Using scale=\"row\" or scale=\"column\" when breaks are",
            "specified can produce unpredictable results.", "Please consider using only one or the other.")
  if (is.null(Rowv) || is.na(Rowv))
    Rowv <- FALSE
  if (is.null(Colv) || is.na(Colv))
    Colv <- FALSE
  else if (Colv == "Rowv" && !isTRUE(Rowv))
    Colv <- FALSE
  if (length(di <- dim(x)) != 2 || !is.numeric(x))
    stop("`x' must be a numeric matrix")
  nr <- di[1]
  nc <- di[2]
  if (nr <= 1 || nc <= 1)
    stop("`x' must have at least 2 rows and 2 columns")
  if (!is.numeric(margins) || length(margins) != 2)
    stop("`margins' must be a numeric vector of length 2")
  if (missing(cellnote))
    cellnote <- matrix("", ncol = ncol(x), nrow = nrow(x))
  if (!(inherits(Rowv, "dendrogram") || inherits(Rowv, "pvclust"))) {
    if (((!isTRUE(Rowv)) || (is.null(Rowv))) && (dendrogram %in%
                                                 c("both", "row"))) {
      if (is.logical(Colv) && (Colv))
        dendrogram <- "column"
      else dedrogram <- "none"
      warning("Discrepancy: Rowv is FALSE, while dendrogram is `",
              dendrogram, "'. Omitting row dendogram.")
    }
  }
  if (!(inherits(Colv, "dendrogram") || inherits(Colv, "pvclust"))) {
    if (((!isTRUE(Colv)) || (is.null(Colv))) && (dendrogram %in%
                                                 c("both", "column"))) {
      if (is.logical(Rowv) && (Rowv))
        dendrogram <- "row"
      else dendrogram <- "none"
      warning("Discrepancy: Colv is FALSE, while dendrogram is `",
              dendrogram, "'. Omitting column dendogram.")
    }
  }
  if (inherits(Rowv, "dendrogram")) {
    ddr <- Rowv
    rowInd <- order.dendrogram(ddr)
  }
  else if (inherits(Rowv, "pvclust")) {
    ddr <- as.dendrogram(Rowv$hclust)
    rowInd <- order.dendrogram(ddr)
  }
  else if (is.integer(Rowv)) {
    hcr <- hclustfun(distfun(x))
    ddr <- as.dendrogram(hcr)
    ddr <- reorder(ddr, Rowv)
    rowInd <- order.dendrogram(ddr)
    if (nr != length(rowInd))
      stop("row dendrogram ordering gave index of wrong length")
  }
  else if (isTRUE(Rowv)) {
    Rowv <- rowMeans(x, na.rm = na.rm)
    hcr <- hclustfun(distfun(x))
    ddr <- as.dendrogram(hcr)
    ddr <- reorder(ddr, Rowv)
    rowInd <- order.dendrogram(ddr)
    if (nr != length(rowInd))
      stop("row dendrogram ordering gave index of wrong length")
  }
  else {
    rowInd <- nr:1
  }
  if (inherits(Colv, "dendrogram")) {
    ddc <- Colv
    colInd <- order.dendrogram(ddc)
  }
  else if (inherits(Colv, "pvclust")) {
    ddc <- as.dendrogram(Colv$hclust)
    colInd <- order.dendrogram(ddc)
  }
  else if (identical(Colv, "Rowv")) {
    if (nr != nc)
      stop("Colv = \"Rowv\" but nrow(x) != ncol(x)")
    if (exists("ddr")) {
      ddc <- ddr
      colInd <- order.dendrogram(ddc)
    }
    else colInd <- rowInd
  }
  else if (is.integer(Colv)) {
    hcc <- hclustfun(distfun(if (symm)
                             x
    else t(x)))
    ddc <- as.dendrogram(hcc)
    ddc <- reorder(ddc, Colv)
    colInd <- order.dendrogram(ddc)
    if (nc != length(colInd))
      stop("column dendrogram ordering gave index of wrong length")
  }
  else if (isTRUE(Colv)) {
    Colv <- colMeans(x, na.rm = na.rm)
    hcc <- hclustfun(distfun(if (symm)
                             x
    else t(x)))
    ddc <- as.dendrogram(hcc)
    ddc <- reorder(ddc, Colv)
    colInd <- order.dendrogram(ddc)
    if (nc != length(colInd))
      stop("column dendrogram ordering gave index of wrong length")
  }
  else {
    colInd <- 1:nc
  }
  retval$rowInd <- rowInd
  retval$colInd <- colInd
  retval$call <- match.call()
  x <- x[rowInd, colInd]
  x.unscaled <- x
  cellnote <- cellnote[rowInd, colInd]
  if (is.null(labRow))
    labRow <- if (is.null(rownames(x)))
      (1:nr)[rowInd]
    else rownames(x)
  else labRow <- labRow[rowInd]
  if (is.null(labCol))
    labCol <- if (is.null(colnames(x)))
      (1:nc)[colInd]
    else colnames(x)
  else labCol <- labCol[colInd]
  if (scale == "row") {
    retval$rowMeans <- rm <- rowMeans(x, na.rm = na.rm)
    x <- sweep(x, 1, rm)
    retval$rowSDs <- sx <- apply(x, 1, sd, na.rm = na.rm)
    x <- sweep(x, 1, sx, "/")
  }
  else if (scale == "column") {
    retval$colMeans <- rm <- colMeans(x, na.rm = na.rm)
    x <- sweep(x, 2, rm)
    retval$colSDs <- sx <- apply(x, 2, sd, na.rm = na.rm)
    x <- sweep(x, 2, sx, "/")
  }
  if (missing(breaks) || is.null(breaks) || length(breaks) <
      1) {
    if (missing(col) || is.function(col))
      breaks <- 16
    else breaks <- length(col) + 1
  }
  if (length(breaks) == 1) {
    if (!symbreaks)
      breaks <- seq(min(x, na.rm = na.rm), max(x, na.rm = na.rm),
                    length = breaks)
    else {
      extreme <- max(abs(x), na.rm = TRUE)
      breaks <- seq(-extreme, extreme, length = breaks)
    }
  }
  nbr <- length(breaks)
  ncol <- length(breaks) - 1
  if (class(col) == "function")
    col <- col(ncol)
  min.breaks <- min(breaks)
  max.breaks <- max(breaks)
  x[x < min.breaks] <- min.breaks
  x[x > max.breaks] <- max.breaks
  if (missing(lhei) || is.null(lhei))
    lhei <- c(keysize, 4)
  if (missing(lwid) || is.null(lwid))
    lwid <- c(keysize, 4)
  if (missing(lmat) || is.null(lmat)) {
    lmat <- rbind(4:3, 2:1)
    if (!missing(ColSideColors)) {
      if (!is.character(ColSideColors) || length(ColSideColors) !=
          nc)
        stop("'ColSideColors' must be a character vector of length ncol(x)")
      lmat <- rbind(lmat[1, ] + 1, c(NA, 1), lmat[2, ] +
                    1)
      lhei <- c(lhei[1], 0.2, lhei[2])
    }
    if (!missing(RowSideColors)) {
      if (!is.character(RowSideColors) || length(RowSideColors) !=
          nr)
        stop("'RowSideColors' must be a character vector of length nrow(x)")
      lmat <- cbind(lmat[, 1] + 1, c(rep(NA, nrow(lmat) -
                                         1), 1), lmat[, 2] + 1)
      lwid <- c(lwid[1], 0.2, lwid[2])
    }
    lmat[is.na(lmat)] <- 0
  }
  if (length(lhei) != nrow(lmat))
    stop("lhei must have length = nrow(lmat) = ", nrow(lmat))
  if (length(lwid) != ncol(lmat))
    stop("lwid must have length = ncol(lmat) =", ncol(lmat))
  op <- par(no.readonly = TRUE)
  on.exit(par(op))
  layout(lmat, widths = lwid, heights = lhei, respect = FALSE)
  if (!missing(RowSideColors)) {
    par(mar = c(margins[1], 0, 0, 0.5))
    image(rbind(1:nr), col = RowSideColors[rowInd], axes = FALSE)
  }
  if (!missing(ColSideColors)) {
    par(mar = c(0.5, 0, 0, margins[2]))
    image(cbind(1:nc), col = ColSideColors[colInd], axes = FALSE)
  }
  par(mar = c(margins[1], 0, 0, margins[2]))
  x <- t(x)
  cellnote <- t(cellnote)
  if (revC) {
    iy <- nr:1
    if (exists("ddr"))
      ddr <- rev(ddr)
    x <- x[, iy]
    cellnote <- cellnote[, iy]
  }
  else iy <- 1:nr
  image(1:nc, 1:nr, x, xlim = 0.5 + c(0, nc), ylim = 0.5 +
        c(0, nr), axes = FALSE, xlab = "", ylab = "", col = col,
        breaks = breaks, ...)
  retval$carpet <- x
  if (exists("ddr"))
    retval$rowDendrogram <- ddr
  if (exists("ddc"))
    retval$colDendrogram <- ddc
  retval$breaks <- breaks
  retval$col <- col
  if (!gtools::invalid(na.color) & any(is.na(x))) {
    mmat <- ifelse(is.na(x), 1, NA)
    image(1:nc, 1:nr, mmat, axes = FALSE, xlab = "", ylab = "",
          col = na.color, add = TRUE)
  }
  axis(1, 1:nc, labels = FALSE, las = 2, line = -0.5, tick = 0,
       cex.axis = cexCol)
  mtext(side = 1, text = labCol, at = 1:nc, col = col.xlab, cex=cexCol, las = lasCol, line = lineCol)
  if (!is.null(xlab))
    mtext(xlab, side = 1, line = margins[1] - 1.25, cex=cex.xlab)
  axis(4, iy, labels = FALSE, las = 2, line = -0.5, tick = 0,
       cex.axis = cexRow)
  mtext(side = 4, text = labRow, at = iy, col = rev(col.ylab), cex = cexRow, las = lasRow, line = lineRow)
  if (!is.null(ylab))
    mtext(ylab, side = 4, line = margins[2] - 1.25, cex=cex.ylab)
  if (!missing(add.expr))
    eval(substitute(add.expr))
  if (!missing(colsep))
    for (csep in colsep) rect(xleft = csep + 0.5, ybottom = rep(0,
                                                    length(csep)), xright = csep + 0.5 + sepwidth[1],
                              ytop = rep(ncol(x) + 1, csep), lty = 1, lwd = 1,
                              col = sepcolor, border = sepcolor)
  if (!missing(rowsep))
    for (rsep in rowsep) rect(xleft = 0, ybottom = (ncol(x) +
                                1 - rsep) - 0.5, xright = nrow(x) + 1, ytop = (ncol(x) +
                                                   1 - rsep) - 0.5 - sepwidth[2], lty = 1, lwd = 1,
                              col = sepcolor, border = sepcolor)
  min.scale <- min(breaks)
  max.scale <- max(breaks)
  x.scaled <- scale01(t(x), min.scale, max.scale)
  if (trace %in% c("both", "column")) {
    retval$vline <- vline
    vline.vals <- scale01(vline, min.scale, max.scale)
    for (i in colInd) {
      if (!is.null(vline)) {
        abline(v = i - 0.5 + vline.vals, col = linecol,
               lty = 2)
      }
      xv <- rep(i, nrow(x.scaled)) + x.scaled[, i] - 0.5
      xv <- c(xv[1], xv)
      yv <- 1:length(xv) - 0.5
      lines(x = xv, y = yv, lwd = 1, col = tracecol, type = "s")
    }
  }
  if (trace %in% c("both", "row")) {
    retval$hline <- hline
    hline.vals <- scale01(hline, min.scale, max.scale)
    for (i in rowInd) {
      if (!is.null(hline)) {
        abline(h = i + hline, col = linecol, lty = 2)
      }
      yv <- rep(i, ncol(x.scaled)) + x.scaled[i, ] - 0.5
      yv <- rev(c(yv[1], yv))
      xv <- length(yv):1 - 0.5
      lines(x = xv, y = yv, lwd = 1, col = tracecol, type = "s")
    }
  }
  if (!missing(cellnote))
    text(x = c(row(cellnote)), y = c(col(cellnote)), labels = c(cellnote),
         col = notecol, cex = notecex)
  par(mar = c(margins[1], if (!is.null(ylableft)) 1 else 0, 0, 0))
  ##par(mar = c(margins[1], 0, 0, 0))
  if (dendrogram %in% c("both", "row")) {
    if (!inherits(Rowv, "pvclust")) {
      plot(ddr, horiz = TRUE, axes = FALSE, yaxs = "i", leaflab = "none", lwd=1)
    } else {
      plot(ddr, horiz = TRUE, axes = FALSE, yaxs = "i", xaxs = "i", leaflab = "none", lwd=1)
      text(Rowv, horiz=TRUE)
      pvrect(Rowv, horiz = TRUE, col=rgb(1, 0, 0, alpha=0.2))
    }
    if (!is.null(ylableft))
      mtext(ylableft, 2, cex=cex.ylableft)
  }
  else plot.new()
  par(mar = c(0, 0, (if (!is.null(xlabtop)) 1 else 0) + (if (!is.null(main)) 5 else 0), margins[2]))
  if (dendrogram %in% c("both", "column")) {
    if (!inherits(Rowv, "pvclust")) {
      plot(ddc, axes = FALSE, xaxs = "i", leaflab = "none")
    } else {
      plot(ddc, horiz = FALSE, axes = FALSE, yaxs = "i", xaxs = "i", leaflab = "none", lwd=1)
      text(Colv, horiz=FALSE)
      pvrect(Colv, horiz = FALSE, col=rgb(1, 0, 0, alpha=0.2))
    }
    if (!is.null(xlabtop))
      mtext(xlabtop, 3, cex=cex.xlabtop)
  }
  else plot.new()
  if (!is.null(main))
    title(main, cex.main = 1.5 * op[["cex.main"]])
  if (key) {
    if (rawkey=="") {
      par(mar = c(5, 4, 2, 1), cex = 0.75)
    } else if (rawkey=="single") {
      par(mar = c(3, 1, 2, 1), cex = 0.4)
    } else if (rawkey == "double") {
      par(mar = c(2, 1, 3, 1), cex = 0.5)
    }
    tmpbreaks <- breaks
    if (symkey) {
      max.raw <- max(abs(c(x, breaks)), na.rm = TRUE)
      min.raw <- -max.raw
      tmpbreaks[1] <- -max(abs(x), na.rm = TRUE)
      tmpbreaks[length(tmpbreaks)] <- max(abs(x), na.rm = TRUE)
    }
    else {
      min.raw <- min(x, na.rm = TRUE)
      max.raw <- max(x, na.rm = TRUE)
    }
    z <- seq(min.raw, max.raw, length = length(col))
    image(z = matrix(z, ncol = 1), col = col, breaks = tmpbreaks,
          xaxt = "n", yaxt = "n")
    par(usr = c(0, 1, 0, 1))
    lv <- pretty(breaks)
    xv <- scale01(as.numeric(lv), min.raw, max.raw)
    axis(1, at = xv, labels = lv)
    if (rawkey == "") {
      if (scale == "row")
        mtext(side = 1, "Row Z-Score", line = 2)
      else if (scale == "column")
        mtext(side = 1, "Column Z-Score", line = 2)
      else mtext(side = 1, "Value", line = 2)
    }
    if (density.info == "density") {
      dens <- density(x, adjust = densadj, na.rm = TRUE)
      omit <- dens$x < min(breaks) | dens$x > max(breaks)
      dens$x <- dens$x[-omit]
      dens$y <- dens$y[-omit]
      dens$x <- scale01(dens$x, min.raw, max.raw)
      lines(dens$x, dens$y/max(dens$y) * 0.95, col = denscol,
            lwd = 1)
      if (is.null(keytitle)) title("Color Key\nand Density Plot") else title(keytitle)
      if (rawkey == "") {
        axis(2, at = pretty(dens$y)/max(dens$y) * 0.95, pretty(dens$y))
        par(cex = 0.5)
        mtext(side = 2, "Density", line = 2)
      }
    }
    else if (density.info == "histogram") {
      h <- hist(x, plot = FALSE, breaks = breaks)
      hx <- scale01(breaks, min.raw, max.raw)
      hy <- c(h$counts, h$counts[length(h$counts)])
      lines(hx, hy/max(hy) * 0.95, lwd = 1, type = "s",
            col = denscol)
      if (is.null(keytitle)) title("Color Key\nand Histogram") else title(keytitle)
      if (rawkey == "") {
        axis(2, at = pretty(hy)/max(hy) * 0.95, pretty(hy))
        par(cex = 0.5)
        mtext(side = 2, "Count", line = 2)
      }
    }
    else {
      if (is.null(keytitle)) title("Color Key") else title(keytitle)
    }
  }
  else plot.new()
  retval$colorTable <- data.frame(low = retval$breaks[-length(retval$breaks)],
                                  high = retval$breaks[-1], color = retval$col)
  invisible(retval)
}

##' This is a version of the \code{head()} function which restricts
##' the number of rows/cols to be printed
##'
##' @title head with restricted dimensions
##' @param mdf the data.frame or matrix to display
##' @param k integer (= 5). the maximal number of rows and/or cols to print
##' @param corner character. in c("NE", "NW", "SE", "SW").  the corner
##'   of the data.frame to print.  defaults to "NE" which will print
##'   the top left corner
##' @return the restricted mdf
##' @author Andreas Leha
##' @export
##' @examples
##' head(mtcars)
##' andhead(mtcars)
##' andhead(mtcars, corner="SW")
##' andhead(mtcars, 11)
##' andhead(mtcars, 20)
andhead <- function(mdf, k=5, corner="NE") {
  lncol <- min(k, ncol(mdf))
  lnrow <- min(k, nrow(mdf))

  if (corner == "NE")
    mdf[1:lnrow, 1:lncol]
  else if (corner == "NW")
    mdf[1:lnrow, ((ncol(mdf)-lncol)+1):ncol(mdf)]
  else if (corner == "SE")
    mdf[((nrow(mdf)-lnrow)+1):nrow(mdf), 1:lncol]
  else if (corner == "SW")
    mdf[((nrow(mdf)-lnrow)+1):nrow(mdf), ((ncol(mdf)-lncol)+1):ncol(mdf)]
}

##' Copied from the vignette of the \code{colorspace} package.  Displays
##' the given colors as stripes.
##'
##' @title visualizing color palettes
##' @param col vector of colors
##' @param border color
##' @param ... passed to plot()
##' @return nothing (important)
##' @author Andreas Leha
##' @export
pal <- function(col, border = "light gray", ...)
{
  n <- length(col)
  plot(0, 0, type="n", xlim = c(0, 1), ylim = c(0, 1),
       axes = FALSE, xlab = "", ylab = "", ...)
  rect(0:(n-1)/n, 0, 1:n/n, 1, col = col, border = border)
}

##' Empty the warnings 'list'.  Copied from http://stackoverflow.com/questions/5725106/r-how-to-clear-all-warnings
##'
##' @title Empty the warnings 'list'
##' @return nothing (important)
##' @author Andreas Leha
##' @export
flushwarnings <- function()
{
  assign("last.warning", NULL, envir = baseenv())
}


##' @export
median.ordered <- function(x, na.rm=FALSE)
{
  if (na.rm) {
    x <- na.omit(x)
  }
  levs <- levels(x)
  m <- median(as.integer(x))
  if(floor(m) != m)
  {
    warning("Median is between two values; using the first one")
    m <- floor(m)
  }
  ordered(m, labels = levs, levels = seq_along(levs))
}

##' Last element of vector (or data.frame)
##' From: http://stackoverflow.com/questions/77434/how-to-access-the-last-value-in-a-vector
##'
##' @title Shorthand to extract the last element of a vector
##' @param x the vector
##' @return the last element of the vector
##' @author Andreas Leha
##' @export
last <- function(x) { tail(x, n = 1) }

##' Evaluates an expression with temporarily changed options.
##' This was suggested on R-devel by Thomas Lumley in a
##' thread started by Charles Geyer.
##'
##' @title evaluate an expression with temporarily changed options
##' @param optlist a named list with the options and their to-be-used values
##' @param expr the expression to evaluate
##' @return the return of expression
##' @author Andreas Leha
##' @export
##' @examples
##' print((1:10)^-1)
##' withOptions(list(digits=3), print((1:10)^-1))
withOptions <- function(optlist, expr)
{
  oldopt <- options(optlist)
  on.exit(options(oldopt))
  expr <- substitute(expr)
  eval.parent(expr)
}

##' Check whether a package is available without attaching it
##'
##' The (before all the namespace importing) suggested way to check
##' whether a package is available used to be
##' \code{if(!require(mypackage)) stop ("need mypackage")}.
##'
##' This has the side-effect of attaching the package, which is not
##' desired in some (most?) cases, when a function of package A wants
##' to check the avialability of package B which is suggested by package A.
##'
##' This code was suggested by Martin Morgan on R-Devel here:
##' http://permalink.gmane.org/gmane.comp.lang.r.devel/32886
##' @title Check the Availability of a Package
##' @param pack character.  The package to check.
##' @return boolean.  TRUE if the package is available.
##' @author Andreas Leha
##' @export
##' @examples
##' packageAvailable("MASS")
##' packageAvailable("MUSS")
packageAvailable <- function(pack)
{
  tryCatch({
    loadNamespace(pack)
    TRUE
  }, error=function(...) FALSE)
}


##' remove user installed packages when also installed via apt
##'
##' The policy in debian systems is to install R packages into the library at
##' /usr/lib/R/site-library when installed via apt-get.
##' There is also a system wide but user writable library in
##' /usr/local/lib/R/site-library.
##' When a package is installed in the latter it (usually) shadows the one
##' installed via apt.
##' This function takes the (crude) approach to delete all packages from the
##' user writable library, if they are also installed via apt.
##'
##' @param user_path character.  Path to the library that should be cleaned.
##' Defaults to "/usr/local/lib/R/site-library/".
##' @param apt_path character.  Path to the system library that serves as
##' 'reference'.
##' Defaults to "/usr/lib/R/site-library/".
##' @return This function is called for its side effect.
##' @author Andreas Leha
##' @export
##' @examples
##' ## to clean the system wite user writable library, run:
##' \dontrun{
##' cleanSyswideUserPackages()
##' }
##'
##' ## to clean the user's personal library, run sth like:
##' \dontrun{
##' cleanSyswideUserPackages("~/R/x86_64-pc-linux-gnu-library/3.0/")
##' }
##'
##' ## to clean the user's personal library wrt to system wide user library:
##' \dontrun{
##' cleanSyswideUserPackages("~/R/x86_64-pc-linux-gnu-library/3.0/",
##'                          "/usr/local/lib/R/site-library/")
##' }
cleanSyswideUserPackages <- function(user_path="/usr/local/lib/R/site-library/",
                                     apt_path="/usr/lib/R/site-library/")
{
  syswide_apt_packages <- dir(apt_path)
  syswide_user_packages <- dir(user_path)

  idx.both <- syswide_user_packages %in% syswide_apt_packages
  to_remove <- syswide_user_packages[idx.both]
  for (user_package in to_remove) {
    message("removing ", user_package, " from ", user_path, "...")
    rm_status <- unlink(file.path(user_path, user_package), recursive = TRUE)

    if (rm_status == 0) {
      message("...success")
    } else {
      message("...failure")
    }
  }
}

##' Generate a data.frame/matrix query function
##'
##' A quick way of returning the entries in column Y where
##' colum X has a given value.
##' This is a closure, that returns a function.  That function
##' take only one argument: the value that column X should have.
##'
##' @param data data.frame or matrix
##' @param querycol numeric or character.
##' This gives the column where the matching happens.
##' If numeric interpreted as column number otherwise as column name.
##' @param rescol numeric or character.
##' This gives the column of which the results are returned.
##' If numeric interpreted as column number otherwise as column name.
##' @return function with only one argument
##' @author Andreas Leha
##' @export
##' @examples
##' ## generate some sample data
##' mydata <- data.frame(a=rep(1:3, each=3), b=1:9)
##' ## have a look
##' mydata
##'
##' ## create the accessor function
##' myfun <- dfGetRowWhereColumn(mydata, "a", "b")
##' ## use it:
##' myfun(2)
##' myfun(2:4)
dfGetRowWhereColumn <- function(data, querycol, rescol) {
  ## check if querycol and rescol exist in data
  dataname <- deparse(substitute(data))
  ## querycol
  if (is.numeric(querycol)) {
    if (querycol > ncol(data)) stop(message("querycol is ", querycol, ", but ", dataname, " has only ", ncol(dataname), " columns"))
  } else {
    if (!querycol %in% colnames(data)) stop(message("there is no column '", querycol, "' in ", dataname))
  }
  ## rescol
  if (is.numeric(rescol)) {
    if (rescol > ncol(data)) stop(message("rescol is ", rescol, ", but ", dataname, " has only ", ncol(dataname), " columns"))
  } else {
    if (!rescol %in% colnames(data)) stop(message("there is no column '", rescol, "' in ", dataname))
  }

  ## remove lines where querycol is NA
  data <- data[!is.na(data[,querycol]),]

  function(entries) {
     res <-
      lapply(entries, function(entry) {
        if (is.na(entry)) {                               ## check if entry is NA
          NA
        } else if (!entry %in% data[,querycol]) {         ## check if querycol contains the entry
          warning("removed entry '", entry, "' from query")
          NA
        } else {
          data[,rescol][data[,querycol]==entry]
        }
      })
    names(res) <- entries

    if (length(entries) == 1) {
      res <- unlist(res)
      names(res) <- NULL
    } else {
      if (all(unlist(lapply(res, length)) < 2)) {
        res <- unlist(res)
        names(res) <- entries
      }
    }

    res
  }
}

##' from ?toupper
##' and the better, more sophisticated version
##'
##' @title capitalize words
##' @param s the string
##' @param strict boolean.  apply tolower() to the rest of the word?
##' @return character.  the capitalized string
##' @author the author of ?tolower
##' @export
capwords <- function(s, strict = FALSE) {
  cap <- function(s) paste(toupper(substring(s,1,1)),
                         {s <- substring(s,2); if(strict) tolower(s) else s},
                           sep = "", collapse = " " )
  sapply(strsplit(s, split = " "), cap, USE.NAMES = !is.null(names(s)))
}
